# Version : 0.9.0

fix: functions

# Version : 0.8.0

fix: pipeline

# Version : 0.7.0

add: DOCKER_WRAPPER_SERVER_SUFFIX

# Version : 0.6.0

add: network option to server_opts

# Version : 0.5.0

fix: docker

# Version : 0.4.0

remove: docker_wrapper_docker

# Version : 0.3.0

fix: running check

# Version : 0.2.0

fix: map detection

# Version : 0.1.1

fix: variables

# Version : 0.1.0

add: gitlab-ci

